package br.com.sistemasmf.endereco.service;

import br.com.sistemasmf.endereco.client.CEPClient;
import br.com.sistemasmf.endereco.client.CEPDTO;
import br.com.sistemasmf.endereco.model.Endereco;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnderecoService {
    @Autowired
    private CEPClient cepClient;

    public Endereco find(String cep){
        CEPDTO cepdto = cepClient.buscar(cep);
        Endereco endereco = new Endereco();
        endereco.setRua(cepdto.getLogradouro());
        return endereco;
    }

}
