package br.com.sistemasmf.endereco.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep", url = "https://viacep.com.br/ws/")
public interface CEPClient {
    @GetMapping("/{cep}/json/")
    CEPDTO buscar(@PathVariable String cep);

}
