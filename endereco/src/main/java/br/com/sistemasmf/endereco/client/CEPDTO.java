package br.com.sistemasmf.endereco.client;

public class CEPDTO {
    private String logradouro;

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }
}
