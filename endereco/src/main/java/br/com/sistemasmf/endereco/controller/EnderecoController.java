package br.com.sistemasmf.endereco.controller;

import br.com.sistemasmf.endereco.model.Endereco;
import br.com.sistemasmf.endereco.service.EnderecoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/endereco")
public class EnderecoController {
    @Autowired
    private EnderecoService enderecoService;

    @GetMapping("/{cep}")
    public Endereco getEndereco(@PathVariable String cep) {
        return enderecoService.find(cep);
    }

}
