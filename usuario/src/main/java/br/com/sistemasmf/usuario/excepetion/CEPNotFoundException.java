package br.com.sistemasmf.usuario.excepetion;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "CEP Inválido")
public class CEPNotFoundException extends RuntimeException {
}
