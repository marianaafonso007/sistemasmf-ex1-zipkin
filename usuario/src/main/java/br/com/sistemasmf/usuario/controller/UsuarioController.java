package br.com.sistemasmf.usuario.controller;

import br.com.sistemasmf.usuario.model.DTO.UsuarioDTO;
import br.com.sistemasmf.usuario.model.Usuario;
import br.com.sistemasmf.usuario.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("/{nome}/{cep}")
    public Usuario getEndereco(@PathVariable String nome, @PathVariable String cep) {
        UsuarioDTO usuarioDTO = new UsuarioDTO(nome, cep);
        return usuarioService.create(usuarioDTO);
    }
}
