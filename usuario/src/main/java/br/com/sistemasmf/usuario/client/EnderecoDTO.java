package br.com.sistemasmf.usuario.client;

public class EnderecoDTO {
    private String rua;

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }
}
