package br.com.sistemasmf.usuario.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "Endereco")
public interface EnderecoClient {
    @GetMapping("endereco/{cep}")
    public EnderecoDTO getEndereco(@PathVariable String cep);
}
