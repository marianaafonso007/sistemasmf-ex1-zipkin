package br.com.sistemasmf.usuario.service;

import br.com.sistemasmf.usuario.client.EnderecoClient;
import br.com.sistemasmf.usuario.client.EnderecoDTO;
import br.com.sistemasmf.usuario.excepetion.CEPNotFoundException;
import br.com.sistemasmf.usuario.model.DTO.UsuarioDTO;
import br.com.sistemasmf.usuario.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {
    @Autowired
    private EnderecoClient enderecoClient;

    public Usuario create(UsuarioDTO usuarioDTO){
        if(usuarioDTO.getCep().length() != 8){
            throw new CEPNotFoundException();
        }else {
            Usuario usuario = new Usuario();
            EnderecoDTO enderecoDTO = enderecoClient.getEndereco(usuarioDTO.getCep());
            usuario.setRua(enderecoDTO.getRua());
            usuario.setNome(usuarioDTO.getNome());
            return usuario;
        }
    }
}
