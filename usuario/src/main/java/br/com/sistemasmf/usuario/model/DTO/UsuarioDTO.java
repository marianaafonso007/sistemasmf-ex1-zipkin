package br.com.sistemasmf.usuario.model.DTO;

public class UsuarioDTO {
    private String nome;
    private String cep;

    public String getNome() {
        return nome;
    }

    public UsuarioDTO() {
    }

    public UsuarioDTO(String nome, String cep) {
        this.nome = nome;
        this.cep = cep;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }
}
